﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthApp.Core.Exceptions
{
    public class GetBloodDetailsException : Exception
    {
        public GetBloodDetailsException(string message) : base(message)
        {
        }
    }
}
