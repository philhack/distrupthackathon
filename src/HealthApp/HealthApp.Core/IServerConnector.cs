﻿using System.IO;

namespace HealthApp.Core
{
    public interface IServerConnector
    {
        void PostImageToServer(MemoryStream ms);

    }
}