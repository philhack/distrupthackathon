﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthApp.Core.Dto
{
    public class TestResultDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string MeasurementUnit { get; set; }
        public string ScanGuid { get; set; }
    }
}
