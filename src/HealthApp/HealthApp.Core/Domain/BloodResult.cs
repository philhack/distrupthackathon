using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace HealthApp.Core.Domain
{

    //{"Id":0,"Patient":"Fabrice Guillermin","IronOriginal":"20","Iron":20.0,"FerritinOriginal":"28","Ferritin":28.0,"DatePosted":null,"DateCreated":"2014-10-18T22:25:02.1111431Z"}
    public class BloodResult
    {
        public int Id { get; set; }
        public string Patient { get; set; }
        public string IronOriginal { get; set; }
        public string Iron { get; set; }
        public string FerritinOriginal { get; set; }
        public string Ferritin { get; set; }
        public DateTime DatePosted { get; set; }
        public DateTime DateCreated { get; set; }
    }
}