namespace HealthApp.Core.Server
{
    public static class UriConfig
    {
        public static string BaseUri()
        {
            return "http://api.healthunleashed.net/api";
        }
    }
}
