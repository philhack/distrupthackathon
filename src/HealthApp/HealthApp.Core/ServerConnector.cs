﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HealthApp.Core.Server;
using ServiceStack;

namespace HealthApp.Core
{
    public class ServerConnector : IServerConnector
    {
        public void  PostImageToServer(MemoryStream ms)
        {
            JsonServiceClient client = new JsonServiceClient();
            var result = client.PostFile<string>(UriConfig.BaseUri() + "/upload", ms, "foo.jpg", "image/jpeg");
        }
    }
}
