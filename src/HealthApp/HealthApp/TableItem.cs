namespace HealthApp
{
    public  class TableItem
    {
        public string Heading { get; set; }
        public string SubHeading { get; set; }
        public string ScanGuid { get; set; }
        public string Value { get; set; }
        public string MeasurementUnit { get; set; }
        public int Id { get; set; }
    }
}