using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealthApp.Core.Domain;
using HealthApp.Core.Exceptions;
using HealthApp.Core.Server;
using HealthApp.Results;
using Java.IO;
using Java.Lang;
using Newtonsoft.Json;
using Environment = Android.OS.Environment;
using Exception = System.Exception;
using String = System.String;
using Uri = Android.Net.Uri;

namespace HealthApp
{
    [Activity(Label = "Scan Test Results")]
    public class ScanActivity : Activity
    {
        private bool DEMO_MODE = false;
        private string DEMO_GUID = "";
        private string _selectedRegion;
        private Button _scanNowButton;
        private ImageView _imageView;
        private ProgressDialog _progress;
        private string _baseServerUri = UriConfig.BaseUri();
        private HttpClient _httpClient;

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            DEMO_MODE = false;
            try
            {
                
                var mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                var contentUri = Uri.FromFile(FileContainer.File);
                mediaScanIntent.SetData(contentUri);
                SendBroadcast(mediaScanIntent);

                var file = new File(FileContainer.File.Path);
                if (DEMO_MODE)
                {
                    DEMO_GUID = Guid.NewGuid().ToString();
                    file = new File("/storage/emulated/0/Pictures/HealthApp/DEMO.jpg");
                }
              
                _progress.Show();
                var uploadResult = await UploadFileToServer(file);

                if (uploadResult is UploadSuccessResult)
                {
                    DisplayToast("Test results have been uploaded");

                    _progress.Hide();
                    if (!String.IsNullOrEmpty(file.Name))
                    {
                        var scanGuid = System.IO.Path.GetFileNameWithoutExtension(file.Name);

                            var intent = new Intent(this, typeof(ConfirmResultsActivity));


                        if (DEMO_MODE)
                        {
                            scanGuid = DEMO_GUID;
                        }


                            intent.PutExtra("ScanGuid", scanGuid);
                            Thread.Sleep(4000);
                            StartActivity(intent); 
                          

                          //List<string> scanGuids = new List<string>();
                          //scanGuids.Add(scanGuid);
                          //intent.PutStringArrayListExtra("ScanGuid", scanGuids);
                          
                    }
                    else
                    {
                        DisplayToast("Unable to view results. There is no GUID accociated with this Scan.");
                    }
                    

                    
                }
                if (uploadResult is UploadFailureResult)
                {
                    var uploadFailure = (UploadFailureResult) uploadResult;
                    _progress.Hide();
                    DisplayToast(uploadFailure.Message);
                }
            }
            catch (Exception ex)
            {
                _progress.Hide();
                DisplayToast("Error" + ex.Message);
                StartActivity(typeof(MainActivity));
            }
            finally
            {
                _progress.Dismiss();
            }
            
        }

        private async Task<UploadResult> UploadFileToServer(File file)
        {

            using (var stream = System.IO.File.OpenRead(file.Path))
            {
                HttpResponseMessage response;
                var content = new MultipartFormDataContent();
                var streamContent = new StreamContent(stream);
                streamContent.Headers.Add("Disrupt", "");

                streamContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");

                if (DEMO_MODE)
                {
                    var fakeFileName = DEMO_GUID + ".jpg";
                    content.Add(streamContent, "image", fakeFileName );
                }
                else
                {
                    content.Add(streamContent, "image", file.Name); 
                }
                

                try
                {
                    
                    response = await _httpClient.PostAsync(UriConfig.BaseUri() + "/upload", content);
                }
                catch (Exception ex)
                {
                    return new UploadFailureResult(ex.Message);
                }

                if (!response.IsSuccessStatusCode)
                {
                    return new UploadFailureResult("Upload Failed:" + response.StatusCode);
                }
                return new UploadSuccessResult();
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Scan);

            _httpClient = new HttpClient();

            _progress = new ProgressDialog(this);
            _progress.SetCancelable(false);
            _progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            _progress.SetTitle("Uploading Test Results to Server");

            _scanNowButton = FindViewById<Button>(Resource.Id.scan_now_button_scan_activity);
            _scanNowButton.SetBackgroundResource(Resource.Drawable.long_blue_button_selector);



            var spinner = FindViewById<Spinner>(Resource.Id.select_region_spinner);

            spinner.ItemSelected += spinner_ItemSelected;
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.regions_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;


            if (IsThereAnAppToTakePictures())
            {
                CreateDirectoryForPictures();

                if (FileContainer.Bitmap != null)
                {
                    _imageView.SetImageBitmap(FileContainer.Bitmap);
                    FileContainer.Bitmap = null;
                }
                _scanNowButton.Click += TakePicture;
            }
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = (Spinner)sender;
            _selectedRegion = spinner.GetItemAtPosition(e.Position).ToString();
        }

        private void CreateDirectoryForPictures()
        {
            FileContainer.Directory = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "HealthApp");
            if (!FileContainer.Directory.Exists())
            {
                FileContainer.Directory.Mkdirs();
            }
        }

        private bool IsThereAnAppToTakePictures()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

        private void TakePicture(object sender, EventArgs eventArgs)
        {
            var intent = new Intent(MediaStore.ActionImageCapture);

            FileContainer.File = new File(FileContainer.Directory, String.Format("{0}.jpg", Guid.NewGuid()));

            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(FileContainer.File));

            StartActivityForResult(intent, 0);
        }

        private void DisplayToast(string message)
        {
            Toast.MakeText(this, message, ToastLength.Long).Show();
        }

    }
}