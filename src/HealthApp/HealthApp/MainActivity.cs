﻿
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace HealthApp
{
    [Activity(Label = "Health Unleashed")]
    public class MainActivity : Activity
    {
        private Button _scanButton;
        private Button _resultsButton;
        private Button _shareButton;
        private Button _logoutButton;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            _scanButton = FindViewById<Button>(Resource.Id.scan_button_main);
            _scanButton.SetBackgroundResource(Resource.Drawable.scanIcon_selector);
            _scanButton.Click += ScanButtonClicked;

            _scanButton = FindViewById<Button>(Resource.Id.results_button_main);
            _scanButton.SetBackgroundResource(Resource.Drawable.resultsIcon_selector);
            _scanButton.Click += ResultsButtonClicked;

            _shareButton = FindViewById<Button>(Resource.Id.share_button_main);
            _shareButton.SetBackgroundResource(Resource.Drawable.shareIcon_selector);
            _shareButton.Click += ShareButtonClicked;

            _logoutButton = FindViewById<Button>(Resource.Id.logout_button_main);
            _logoutButton.SetBackgroundResource(Resource.Drawable.logoutIcon_selector);
            _logoutButton.Click += LogoutButtonClicked;
        }

        private void LogoutButtonClicked(object sender, EventArgs e)
        {
            StartActivity(typeof(LoginActivity));
        }

        private void ShareButtonClicked(object sender, EventArgs e)
        {
            StartActivity(typeof(ShareActivity));     
        }

        private void ResultsButtonClicked(object sender, EventArgs e)
        {
            StartActivity(typeof(ResultsActivity));
        }

        private void ScanButtonClicked(object sender, EventArgs e)
        {
            StartActivity(typeof(ScanActivity));
        }
    }
}

