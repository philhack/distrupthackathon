using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HealthApp.Results
{
    public class UploadFailureResult : UploadResult
    {
        public UploadFailureResult(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
    }
}