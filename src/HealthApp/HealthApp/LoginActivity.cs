using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HealthApp
{
    [Activity(Label = "Login", MainLauncher = true)]
    public class LoginActivity : Activity
    {
        private Button _loginButton;
        private EditText _usernameEditText;
        private EditText _passwordEditText;
        private ImageView _imageView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Login);


            _loginButton = FindViewById<Button>(Resource.Id.login_button);
            _loginButton.SetBackgroundResource(Resource.Drawable.long_blue_button_selector);
            _loginButton.Click += LoginButtonClicked;

            _usernameEditText = FindViewById<EditText>(Resource.Id.username_text_login);
            _usernameEditText.Gravity = GravityFlags.Center;

            _passwordEditText = FindViewById<EditText>(Resource.Id.password_text_login);
            _passwordEditText.Gravity = GravityFlags.Center;



        }

        private void LoginButtonClicked(object sender, EventArgs e)
        {
            StartActivity(typeof(MainActivity));
        }
    }
}