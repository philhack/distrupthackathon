using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealthApp.Core.Dto;
using HealthApp.Core.Server;
using Newtonsoft.Json;

namespace HealthApp
{
    [Activity(Label = "UpdateSingleTestResultActivity")]
    public class UpdateSingleTestResultActivity : Activity
    {
        private Button _updateButton;
        private EditText _descriptionTextView;
        private EditText _valueEditText;
        private EditText _measurementEditText;
        private string _scanGuid;
        private HttpClient _httpClient;
        private ProgressDialog _progress;

        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.UpdateSingleTestResult);
            _httpClient = new HttpClient();


            _progress = new ProgressDialog(this);
            _progress.SetCancelable(false);
            _progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            _progress.SetTitle("Updating");

            _updateButton = FindViewById<Button>(Resource.Id.update_single_result_button);          
            _updateButton.SetBackgroundResource(Resource.Drawable.long_blue_button_selector);
            
            _descriptionTextView = FindViewById<EditText>(Resource.Id.description_update_single_result);
            _valueEditText = FindViewById<EditText>(Resource.Id.value_update_single_result);
            _measurementEditText = FindViewById<EditText>(Resource.Id.measurement_update_single_result);


            _updateButton.Click += UpdateButtonClicked;

            
            var jsonPayload = Intent.GetStringExtra("JsonPayload") ?? "";
            if (!String.IsNullOrEmpty(jsonPayload))
            {
                _progress.Show();
                var testResulDto =
                    await
                        Task.Factory.StartNew(
                            () =>
                                JsonConvert.DeserializeObject<TestResultDto>(jsonPayload,
                                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore}));
                _descriptionTextView.Text = testResulDto.Description;
                _valueEditText.Text = testResulDto.Value;
                _measurementEditText.Text = testResulDto.MeasurementUnit;
                _scanGuid = testResulDto.ScanGuid;
                _progress.Hide();
            }
            else
            {
                Toast.MakeText(this, "Unable to edit this test result", ToastLength.Long).Show();
                _updateButton.Enabled = false;
            }
        }


        private async void UpdateButtonClicked(object sender, EventArgs e)
        {

            try
            {
                _progress.Show();
                string stringPayload = "";
                if (_descriptionTextView.Text.Equals("Iron"))
                {
                    var iron = new {Iron = _valueEditText.Text};
                    stringPayload = JsonConvert.SerializeObject(iron);

                }

                else if (_descriptionTextView.Text.Equals("Ferritin"))
                {
                    var ferritin = new {Ferritin = _valueEditText.Text};
                    stringPayload = JsonConvert.SerializeObject(ferritin);

                }

                await UpdateValueOnServer(stringPayload);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, String.Format("Error: {0}", ex.Message), ToastLength.Long).Show();
            }
            finally
            {
                _progress.Hide();
            }
            
        }

        private async Task UpdateValueOnServer(string stringPayload)
        {
            HttpContent content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            content.Headers.Add("Disrupt", "");

            var response = await _httpClient.PutAsync(UriConfig.BaseUri() + "/blood/update/" + _scanGuid, content);
            if (response.IsSuccessStatusCode)
            {
                Toast.MakeText(this, "Updated", ToastLength.Long).Show();
            }
        }
    }
}