using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HealthApp.Core.Domain;
using HealthApp.Core.Dto;
using HealthApp.Core.Exceptions;
using HealthApp.Core.Server;
using Java.Lang;
using Newtonsoft.Json;
using Exception = System.Exception;
using String = System.String;

namespace HealthApp
{
    [Activity(Label = "Confirm Test Results")]
    public class ConfirmResultsActivity : ListActivity
    {
        readonly List<TableItem> _tableItems = new List<TableItem>();
        private ProgressDialog _progress;
        private HttpClient _httpClient;


        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            _httpClient = new HttpClient();

            //var scanGuid = "e0cd38b0-b0ac-46cd-881b-0506d866f6e7";      // ninja hack
            var scanGuid = Intent.GetStringExtra("ScanGuid") ?? "";
            ListView.ChoiceMode = ChoiceMode.Single;

            try
            {
                var bloodResult = await GetBloodTestDetailsFromServer(scanGuid);
                PopulateListViewWithBloodTestResults(bloodResult, scanGuid);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
            }
            ListAdapter = new HomeScreenAdapter(this, _tableItems);
        }

        private void PopulateListViewWithBloodTestResults(BloodResult bloodResult, string scanGuid)
        {
            _tableItems.Add(new TableItem()
            {
                Id = 1,
                Heading = "Iron",
                SubHeading = bloodResult.Iron + " umol/L",
                Value = bloodResult.Iron,
                MeasurementUnit = "umol/L",
                ScanGuid = scanGuid,
            });
            _tableItems.Add(new TableItem()
            {
                Id = 2,
                Heading = "Ferritin",
                SubHeading = bloodResult.Ferritin + " ug/L",
                Value = bloodResult.Ferritin,
                MeasurementUnit = "ug/L",
                ScanGuid = scanGuid,
            });
        
        }



        private async Task<BloodResult> GetBloodTestDetailsFromServer(string scanGuid)
        {
            _httpClient.DefaultRequestHeaders.Add("Disrupt", "");
            var response = await _httpClient.GetAsync(UriConfig.BaseUri() + "/blood/result/" + scanGuid);
            if (!response.IsSuccessStatusCode)
                throw new GetBloodDetailsException(String.Format("failed to fetch blood result: {0}", response.StatusCode));
            var responseBody = await response.Content.ReadAsStringAsync();


            var bloodResult =
                await
                    Task.Factory.StartNew(
                        () =>
                            JsonConvert.DeserializeObject<BloodResult>(responseBody,
                                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
         
            return bloodResult;
        }

        protected override async void OnListItemClick(ListView l, View v, int position, long id)
        {
            var t = _tableItems[position];
     


            var testResultDto = new TestResultDto
            {
                Description = t.Heading,
                Id = t.Id,
                MeasurementUnit = t.MeasurementUnit,
                Value = t.Value,
                ScanGuid = t.ScanGuid
            };
            

            var serializedTestResultDetails = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(testResultDto, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

            var intent = new Intent(this, typeof(UpdateSingleTestResultActivity));

            intent.PutExtra("JsonPayload", serializedTestResultDetails );
            StartActivity(intent);

        }
    }
}