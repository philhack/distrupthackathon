﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using HealthApp.Core.Domain;
using HealthApp.Core.Server;
using Newtonsoft.Json;
using NUnit.Framework;

namespace HealthApp.Core.Tests.Integration
{
    [TestFixture]
    public class GetResultsTest
    {
        [Test]
        public async void given_a_test_result_that_exists_we_are_able_to_get_the_results()
        {
            var testResultId = "518b4c2f-4dc7-4b9e-b230-a46389966c8d";
            var httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Add("Disrupt", "");
            var response = await httpClient.GetAsync(UriConfig.BaseUri() + "/blood/result/" + testResultId);

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();

            responseBody.Should().NotBeNullOrEmpty();


            var deserializedResults =
                await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<BloodResult>(responseBody, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));


            deserializedResults.Patient.Should().Be("Phil");
            deserializedResults.IronOriginal.Should().BeNull();
            deserializedResults.Iron.Should().Be("58");
            deserializedResults.FerritinOriginal.Should().BeNull();
            deserializedResults.Ferritin.Should().Be("63");
        }
    }
}
