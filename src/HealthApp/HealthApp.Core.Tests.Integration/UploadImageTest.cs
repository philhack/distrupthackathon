﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using HealthApp.Core.Server;
using NUnit.Framework;

namespace HealthApp.Core.Tests.Integration
{
    [TestFixture]
    public class UploadImageTest
    {
        [Test]
        public async void Can_upload_image_to_server()
        {
            HttpResponseMessage response;
            HttpClient httpClient = new HttpClient();


            var imagePath = Path.GetFullPath(@"testimages\blood-test-results.jpg");

            using (var stream = File.OpenRead(imagePath))
            {
                var content = new MultipartFormDataContent();
                var streamContent = new StreamContent(stream);
                streamContent.Headers.Add("Disrupt", "");

                streamContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                var fileName = Guid.NewGuid() + ".jpg";
                content.Add(streamContent, "image", fileName);

                response = await httpClient.PostAsync(UriConfig.BaseUri() + "/upload", content);
            }

            response.IsSuccessStatusCode.Should().BeTrue();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }


    }
}
