﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using HealthApp.Core.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;

namespace HealthApp.Core.Tests.Integration
{
    [TestFixture]
    public class UpdateSingleResultTest
    {
        private HttpClient _httpClient;

        [SetUp]
        public void SetUp()
        {
            _httpClient = new HttpClient();
        }

        [Test]
        public async void can_update_iron()
        {

            var guid = "e0cd38b0-b0ac-46cd-881b-0506d866f6e7";
            var iron = new {Iron = "45"};

            var stringPayload =  await Task.Factory.StartNew(() => JsonConvert.SerializeObject(iron, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));;

            HttpContent content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            content.Headers.Add("Disrupt", "");

            var response = await _httpClient.PutAsync(UriConfig.BaseUri() + "/blood/update/" + guid, content);

            response.IsSuccessStatusCode.Should().Be(true);
        }
    }
}
